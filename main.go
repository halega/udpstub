package main

import (
	"log"
	"net"
	"os"
	"os/signal"
)

func main() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go listen()
	<-c
	log.Print("Exiting...")
	os.Exit(0)
}

func listen() {
	packetConn, err := net.ListenPacket("udp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	defer packetConn.Close()

	buffer := make([]byte, 1024)
	packetConn.ReadFrom(buffer)
}
